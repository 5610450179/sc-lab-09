package Problem2;

import java.util.ArrayList;
import java.util.List;



public class InOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> nodes = new ArrayList<Node>();
		if(node == null)return nodes;
		
		if(node.getLeft() != null)nodes.addAll(traverse(node.getLeft()));
		nodes.add(node);
		if(node.getRight() != null)nodes.addAll(traverse(node.getRight()));
		
		return nodes;
	}
	

}
