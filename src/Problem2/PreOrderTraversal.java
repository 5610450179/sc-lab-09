package Problem2;

import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal{

	@Override
	public List<Node> traverse(Node node) {
		List<Node> nodes = new ArrayList<Node>();
		if(node == null)return nodes;
		
		nodes.add(node);
		if(node.getLeft() != null)nodes.addAll(traverse(node.getLeft()));
		if(node.getRight() != null)nodes.addAll(traverse(node.getRight()));
		
		return nodes;
	}

}
