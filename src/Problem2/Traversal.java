package Problem2;

import java.util.ArrayList;
import java.util.List;

public interface Traversal {
	
	public List<Node> traverse(Node node);

}
