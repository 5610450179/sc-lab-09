package Problem2;

public class TraverseTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node node = new Node("F",new Node("B",new Node("A",null,null),new Node("D",new Node("C",null,null),new Node("E",null,null))),new Node("G",null,new Node("I",new Node("H",null,null),null)));
		System.out.print("Traverse with PreorderTraversal: ");
		ReportConsle.display(node, new PreOrderTraversal());
		System.out.print("\nTraverse with InorderTraversal: ");
		ReportConsle.display(node, new InOrderTraversal());
		System.out.print("\nTraverse with PostorderTraversal: ");
		ReportConsle.display(node, new PostOrderTraversal());

	}

}
