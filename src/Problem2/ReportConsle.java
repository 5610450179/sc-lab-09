package Problem2;

import java.util.List;

public class ReportConsle {
	
	public static void display(Node root,Traversal traversal){
		List<Node> nodes = traversal.traverse(root);
		for(Node node:nodes){
			System.out.print(node.getValue()+" ");
		}
	}

}
