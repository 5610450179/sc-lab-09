package Problem1;


import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	public int compare(Company a, Company b) {
		if(a.getIncome() < b.getIncome()) return -1;
		if(a.getIncome() > b.getIncome()) return 1;
		return 0;
	}
	

}
