package Problem1;


import java.util.ArrayList;
import java.util.Collections;

public class TestCompany {

	public static void main(String[] args) {
		
		ArrayList<Company> companys = new ArrayList<Company>();
		companys.add(new Company("A", 1000000, 900000));
		companys.add(new Company("B", 2000000, 700000));
		companys.add(new Company("C", 900000, 850000));
		
		//EarningComparator
		System.out.println("-- EarningComparator --");
		//Before
		System.out.println("<< Before >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" income: "+obj.getIncome());
		}
		Collections.sort(companys, new EarningComparator());
		//After
		System.out.println("<< After >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" income: "+obj.getIncome());
		}
		//ExpenseComparator
		System.out.println("-- ExpenseComparator --");
		//Before
		System.out.println("<< Before >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" expense: "+obj.getExpense());
		}
		Collections.sort(companys, new ExpenseComparator());
		//After
		System.out.println("<< After >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" expense: "+obj.getExpense());
		}
		//ProfitComparator
		System.out.println("-- ProfitComparator --");
		//Before
		System.out.println("<< Before >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" profit: "+obj.getProfit());
		}
		Collections.sort(companys, new ProfitComparator());
		//After
		System.out.println("<< After >>");
		for(Company obj:companys){
			System.out.println(obj.getName()+" profit: "+obj.getProfit());
		}
		
		
		
		

	}

}
