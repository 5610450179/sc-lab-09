package Problem1;
import java.util.ArrayList;
import java.util.Collections;

public class TestTaxable {

	public static void main(String[] args) {

		ArrayList<Taxable> taxs = new ArrayList<Taxable>();
		taxs.add(new Person("Thanawan", 100000));
		taxs.add(new Company("AIS", 1000000,800000));
		taxs.add(new Product("milk", 100));		
		//Before
		System.out.println("<< Before >>");
		for(Taxable obj:taxs){
			System.out.println(obj.getTax());
		}
		
		Collections.sort(taxs,new TaxableComparator());
		//After
		System.out.println("<< After >>");
		for(Taxable obj:taxs){
			System.out.println(obj.getTax());
		}
		
	}

}
