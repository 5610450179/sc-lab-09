package Problem1;


import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	public int compare(Company a, Company b) {
		if(a.getExpense() < b.getExpense()) return -1;
		if(a.getExpense() > b.getExpense()) return 1;
		return 0;
	}
}
