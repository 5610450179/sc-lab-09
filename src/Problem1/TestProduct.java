package Problem1;


import java.util.ArrayList;
import java.util.Collections;

public class TestProduct {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("milk", 100));
		products.add(new Product("egg", 50));
		products.add(new Product("coke", 300));
		//Before
		System.out.println("<< Before >>");
		for(Product obj:products){
			System.out.println(obj);
		}
		
		Collections.sort(products);
		//After
		System.out.println("<< After >>");
		for(Product obj:products){
			System.out.println(obj);
		}
	}

}
