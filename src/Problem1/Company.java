package Problem1;


public class Company implements Taxable{
	private String name;
	private double income;
	private double expense;
	private double profit;
	
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
		this.profit = income-expense;
	}

	
	public String toString(){
		return name+" income: "+getIncome()+" expense: "+expense;
	}
	
	
	public String getName() {
		return name;
	}

	public double getIncome() {
		return income;
	}

	public double getExpense() {
		return expense;
	}
	
	public double getProfit() {
		return profit;
	}

	@Override
	public double getTax() {
		return (income-expense)*0.3;
	}

}
