package Problem1;
import java.util.Comparator;


public class TaxableComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable a, Taxable b) {
		if(a.getTax() < b.getTax()) return -1;
		if(a.getTax() > b.getTax()) return 1;
		return 0;
	}

	

}
