package Problem1;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TestPerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("ThanaAuu", 100));
		persons.add(new Person("NattaBall", 50));
		persons.add(new Person("ChanNick", 300));
		//Before
		System.out.println("<< Before >>");
		for(Person obj:persons){
			System.out.println(obj);
		}
		
		Collections.sort(persons);
		//After
		System.out.println("<< After >>");
		for(Person obj:persons){
			System.out.println(obj);
		}

	}

}
