package Problem1;


import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{
	
	public int compare(Company a, Company b) {
		if(a.getProfit() < b.getProfit()) return -1;
		if(a.getProfit() > b.getProfit()) return 1;
		return 0;
	}
}
