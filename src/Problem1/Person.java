package Problem1;


public class Person implements Comparable,Taxable{
	
	private String name; 
	private int income;
	public Person(String name,int income){
		this.name = name;
		this.income = income;
	}
	
	public String toString(){
		return name+" income: "+income;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public int compareTo(Object o) {
		Person other = (Person) o;
		if(income < other.income) return -1;
		if(income > other.income) return 1;
		return 0;
	}

	@Override
	public double getTax() {
		if(income<=300000)return income*0.05;
		return (300000*0.05)+((income-300000)*0.1);
	}
	

}
