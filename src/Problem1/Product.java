package Problem1;



public class Product implements Comparable,Taxable{
	private String name;
	private double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	
	public double getTax() {
		return price*0.07;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString(){
		return name+" price: "+price;
	}


	@Override
	public int compareTo(Object o) {
		Product other = (Product) o;
		if(price < other.price) return -1;
		if(price > other.price) return 1;
		return 0;
	}


}
